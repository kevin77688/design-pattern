package org.ntutssl.shapes;

import org.junit.Test;

import static org.junit.Assert.*;

import org.junit.Before;

public class RectangleTest
{
    Rectangle r;
    final double delta = 0.001;

    @Before
    public void Initial(){
        try{
            r = new Rectangle(5, 6);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void GetVariable(){
        assertEquals(5, r.getLength(), delta);
        assertEquals(6, r.getWidth(), delta);
    }

    @Test
    public void Area(){
        assertEquals(30, r.area(), delta);
    }

    @Test
    public void Perimeter(){
        assertEquals(22, r.perimeter(), delta);
    }

    @Test
    public void RectangleToString(){
        assertEquals("Rectangle 5.0 6.0", r.toString());
    }

    @Test
    public void LengthBelowZero(){
        try{
            new Rectangle(-1, 2);
            fail();
        } catch (Exception e){
            assertEquals("It's not a rectangle!", e.getMessage());
        }
    }

    @Test
    public void WidthBelowZero(){
        try{
            new Rectangle(2, -3);
            fail();
        } catch (Exception e){
            assertEquals("It's not a rectangle!", e.getMessage());
        }
    }

}