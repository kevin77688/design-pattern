package org.ntutssl.shapes;

import org.junit.Test;

import static org.junit.Assert.*;

import org.junit.Before;

import java.util.ArrayList;

public class ShapeTest
{
    ArrayList<Measurable> _shapeList = new ArrayList<>();
    Circle circle;
    Rectangle rectangle;
    Triangle triangle;
    ConvexPolygon polygon;
    // Circle       area = 3.14     perimeter = 6.28
    // Rectangle    area = 0.75     perimeter = 4
    // Triangle     area = 6        perimeter = 12
    // Polygon      area = 150      perimeter = 54.14

    @Before
    public void Initial(){
        try{
            circle = new Circle(1.0);
            _shapeList.add(circle);
            rectangle = new Rectangle(0.5, 1.5);
            _shapeList.add(rectangle);
            ArrayList<TwoDimensionalVector> triangleVector = new ArrayList<>();
            triangleVector.add(new TwoDimensionalVector("[4,0]"));
            triangleVector.add(new TwoDimensionalVector("[4,3]"));
            triangleVector.add(new TwoDimensionalVector("[0,3]"));
            triangle = new Triangle(triangleVector);
            _shapeList.add(triangle);
            ArrayList<TwoDimensionalVector> polygonVector = new ArrayList<>();
            polygonVector.add(new TwoDimensionalVector("[0,0]"));
            polygonVector.add(new TwoDimensionalVector("[-8,-6]"));
            polygonVector.add(new TwoDimensionalVector("[-2,-14]"));
            polygonVector.add(new TwoDimensionalVector("[14,-2]"));
            polygon = new ConvexPolygon(polygonVector);
            _shapeList.add(polygon);
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void SortByAreaAscending(){
        _shapeList.sort(Sort.BY_AREA_ASCENDING);
        assertEquals(rectangle, _shapeList.get(0));
        assertEquals(circle, _shapeList.get(1));
        assertEquals(triangle, _shapeList.get(2));
        assertEquals(polygon, _shapeList.get(3));
    }

    @Test
    public void SortByAreaDescending(){
        _shapeList.sort(Sort.BY_AREA_DESCENDING);
        assertEquals(polygon, _shapeList.get(0));
        assertEquals(triangle, _shapeList.get(1));
        assertEquals(circle, _shapeList.get(2));
        assertEquals(rectangle, _shapeList.get(3));
    }

    @Test
    public void SortByPerimeterAscending(){
        _shapeList.sort(Sort.BY_PERIMETER_ASCENDING);
        assertEquals(rectangle, _shapeList.get(0));
        assertEquals(circle, _shapeList.get(1));
        assertEquals(triangle, _shapeList.get(2));
        assertEquals(polygon, _shapeList.get(3));
    }

    @Test
    public void SortByPerimeterDescending(){
        _shapeList.sort(Sort.BY_AREA_DESCENDING);
        assertEquals(polygon, _shapeList.get(0));
        assertEquals(triangle, _shapeList.get(1));
        assertEquals(circle, _shapeList.get(2));
        assertEquals(rectangle, _shapeList.get(3));
    }
}