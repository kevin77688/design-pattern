package org.ntutssl.shapes;

import org.junit.Test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;

public class ConvexPolygonTest 
{
    ArrayList<TwoDimensionalVector> vectors;
    ConvexPolygon p1;
    final double delta = 0.001;

    @Before
    public void Initial(){
        try{
            vectors = new ArrayList<TwoDimensionalVector>();
            vectors.add(new TwoDimensionalVector(0, 0));
            vectors.add(new TwoDimensionalVector(2, 0));
            vectors.add(new TwoDimensionalVector(2, 2));
            vectors.add(new TwoDimensionalVector(0, 2));
            p1 = new ConvexPolygon(vectors);
        } catch (Exception e){
            e.printStackTrace();
        }
        
    }

    @Test
    public void Polygon1_GetVariable(){
        assertEquals(0, p1.at(1).getX());
        assertEquals(0, p1.at(1).getY());
        assertEquals(2, p1.at(2).getX());
        assertEquals(0, p1.at(2).getY());
        assertEquals(2, p1.at(3).getX());
        assertEquals(2, p1.at(3).getY());
        assertEquals(0, p1.at(4).getX());
        assertEquals(2, p1.at(4).getY());
    }

    @Test
    public void Polygon1_Perimeter(){
        assertEquals(8.0, p1.perimeter(), delta);
    }

    @Test
    public void Polygon1_Area(){
        assertEquals(4.0, p1.area(), delta);
    }

    @Test
    public void Polygon1_ConvexPolygonToString(){
        assertEquals("ConvexPolygon [0,0] [2,0] [2,2] [0,2]", p1.toString());
    }

    @Test
    public void ConcavePolygonThrowException(){
        try{
            ArrayList<TwoDimensionalVector> tempVector = new ArrayList<>();
            tempVector.add(new TwoDimensionalVector("[0,0]"));
            tempVector.add(new TwoDimensionalVector("[2,2]"));
            tempVector.add(new TwoDimensionalVector("[2,0]"));
            tempVector.add(new TwoDimensionalVector("[0,2]"));
            new ConvexPolygon(tempVector);
            fail();
        } catch (Exception e){
            assertEquals("It's not a convex polygon!", e.getMessage());
        }
    }

    @Test
    public void CounterClockwisePolygon(){
        try{
            ArrayList<TwoDimensionalVector> tempVector = new ArrayList<>();
            tempVector.add(new TwoDimensionalVector("[5,0]"));
            tempVector.add(new TwoDimensionalVector("[0,0]"));
            tempVector.add(new TwoDimensionalVector("[-3,4]"));
            tempVector.add(new TwoDimensionalVector("[0,8]"));
            tempVector.add(new TwoDimensionalVector("[5,8]"));
            tempVector.add(new TwoDimensionalVector("[8,4]"));

            ConvexPolygon polygon = new ConvexPolygon(tempVector);
            assertEquals(64, polygon.area(), delta);
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}