package org.ntutssl.shapes;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import java.util.ArrayList;

public class TriangleTest {
    ArrayList<TwoDimensionalVector> vectors = new ArrayList<TwoDimensionalVector>();
    Triangle t;
    final double delta = 0.001;

    @Before
    public void Initial() {
        vectors.add(new TwoDimensionalVector(0, 0));
        vectors.add(new TwoDimensionalVector(3, 0));
        vectors.add(new TwoDimensionalVector(3, 4));
        try {
            t = new Triangle(vectors);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    

    @Test
    public void GetVariable(){
        assertEquals(0, t.at(1).getX());
        assertEquals(0, t.at(1).getY());
        assertEquals(3, t.at(2).getX());
        assertEquals(0, t.at(2).getY());
        assertEquals(3, t.at(3).getX());
        assertEquals(4, t.at(3).getY());
    }

    @Test
    public void Area(){
        assertEquals(6.0, t.area(), delta);
    }

    @Test
    public void Perimeter(){
        assertEquals(12.0, t.perimeter(), delta);
    }

    @Test
    public void TriangleToString(){
        assertEquals("Triangle [0,0] [3,0] [3,4]", t.toString());
    }
    @Test
    public void TwoVectorAreParallelThrowException(){
        try{
            ArrayList<TwoDimensionalVector> tepmVector = new ArrayList<TwoDimensionalVector>();
            tepmVector.add(new TwoDimensionalVector("[1,1]"));
            tepmVector.add(new TwoDimensionalVector("[2,2]"));
            tepmVector.add(new TwoDimensionalVector("[0,0]"));
            new Triangle(tepmVector);
            fail();
        } catch (Exception e){
            assertEquals("It's not a triangle!", e.getMessage());
        }
    }

    @Test
    public void VectorOverThree(){
        try{
            ArrayList<TwoDimensionalVector> tepmVector = new ArrayList<TwoDimensionalVector>();
            tepmVector.add(new TwoDimensionalVector("[0,0]"));
            tepmVector.add(new TwoDimensionalVector("[3,0]"));
            tepmVector.add(new TwoDimensionalVector("[3,4]"));
            tepmVector.add(new TwoDimensionalVector("[0,4]"));
            new Triangle(tepmVector);
            fail();
        } catch (Exception e){
            assertEquals("It's not a triangle!", e.getMessage());
        }
        
    }
}