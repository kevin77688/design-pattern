package org.ntutssl.shapes;

import org.junit.Test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Iterator;

import org.junit.Before;

public class CompoundShapeTest{
    private final double delta = 0.001;
    Shape sq1, sq2, cir1;
    CompoundShape cs1, cs2;

    @Before
    public void Initial(){
        try{
            sq1 = new Square(3);
            cir1 = new Circle(100);
            cs2 = new CompoundShape();
            cs2.add(sq1);
            cs2.add(cir1);

            cs1 = new CompoundShape();
            cs1.add(cs2);
            sq2 = new Square(5);
            cs1.add(sq2);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void AddShape(){
        assertEquals(sq1, cs2.getChild(0));
        assertEquals(cir1, cs2.getChild(1));
    }

    @Test
    public void AddShapeToSimpleShapeThrowException(){
        try{
            sq1.add(cir1);
            fail();
        } catch (Exception e) {
            assertEquals("Illegal Operation", e.getMessage());
        }
    }

    @Test
    public void CompoundShapeArea(){
        assertEquals(9 + 31415.9265, cs2.area(), delta);
        assertEquals(9 + 31415.9256 + 25, cs1.area(), delta);
    }

    @Test
    public void areaGreaterThan(){
        ArrayList<Shape> shape = cs1.selectByAreaGreaterThan(10);
        assertEquals(cs1, shape.get(0));
        assertEquals(cs2, shape.get(1));
        assertEquals(cir1, shape.get(2));
        assertEquals(sq2, shape.get(3));
    }

    @Test
    public void test_null_iterator(){
        try{
            Shape cir1 = new Circle(100);
            Iterator<Shape> it = cir1.iterator();
            assertFalse(it.hasNext());
        } catch (Exception e) {
            fail();
        }
    }

}