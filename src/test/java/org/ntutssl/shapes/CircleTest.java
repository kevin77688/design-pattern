package org.ntutssl.shapes;

import org.junit.Test;

import static org.junit.Assert.*;

import org.junit.Before;

public class CircleTest {
    private double delta = 0.001;
    Circle circle;

    @Before
    public void Initial() {
        try {
            circle = new Circle(2.0);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void GetVariable() {
        assertEquals(2, circle.getRadius(), delta);
    }

    @Test
    public void Area() {
        assertEquals(4 * Math.PI, circle.area(), delta);
    }

    @Test
    public void Perimeter() {
        assertEquals(4 * Math.PI, circle.perimeter(), delta);
    }

    @Test
    public void ToString() {
        assertEquals("Circle 2.0", circle.toString());
    }

    @Test
    public void CircleRadiusBelowZero() {
        try {
            new Circle(-1);
            fail();
        } catch (Exception e) {
            assertEquals("It's not a circle!", e.getMessage());
        }
    }
}