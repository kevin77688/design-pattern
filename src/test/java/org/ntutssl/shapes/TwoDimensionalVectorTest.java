package org.ntutssl.shapes;

import org.junit.Test;

import static org.junit.Assert.*;

import org.junit.Before;

public class TwoDimensionalVectorTest
{
    TwoDimensionalVector u, v;
    final double delta = 0.001;

    @Before
    public void Initial(){
        v = new TwoDimensionalVector(3, 4);
        u = new TwoDimensionalVector(1, 1);
    }

    @Test
    public void GetVariable(){
        assertEquals(3, v.getX());
        assertEquals(4, v.getY());
    }

    @Test
    public void VectorToString(){
        assertEquals("[3,4]", v.toString());
        assertEquals("[1,1]", u.toString());
    }

    @Test
    public void Distance(){
        assertEquals(5.0, v.length(), delta);
        double a = Math.sqrt(2);
        assertEquals(a, u.length(), delta);
    }

    @Test
    public void Dot(){
        assertEquals(7, v.dot(u));
    }

    @Test
    public void Cross(){
        assertEquals(-1, v.cross(u));
        assertEquals(1, u.cross(v));
    }

    @Test
    public void Subtract(){
        TwoDimensionalVector answer = v.subtract(u);
        assertEquals(2, answer.getX());
        assertEquals(3, answer.getY());
    }

    @Test
    public void ConstructWithString(){
        TwoDimensionalVector stringVector = new TwoDimensionalVector("[10,13]");
        assertEquals(10, stringVector.getX());
        assertEquals(13, stringVector.getY());
    }

}