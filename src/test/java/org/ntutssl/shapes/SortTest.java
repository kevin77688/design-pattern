package org.ntutssl.shapes;

import org.junit.Test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;

public class SortTest
{
    private Measurable rec1;
    private Measurable cir1;
    private Measurable tri1;
    private ArrayList<Measurable> shapes;

    @Before
    public void Initial(){
        try{
            rec1 = new Rectangle(5, 5);
            cir1 = new Circle(10);
            ArrayList<TwoDimensionalVector> arrayList = new ArrayList<TwoDimensionalVector>();
            arrayList.add(new TwoDimensionalVector(0, 0));
            arrayList.add(new TwoDimensionalVector(3, 0));
            arrayList.add(new TwoDimensionalVector(3, 4));
            tri1 = new Triangle(arrayList);
            shapes = new ArrayList<Measurable>();
            shapes.add(rec1);
            shapes.add(cir1);
            shapes.add(tri1);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void SortAreaByAscending(){
        shapes.sort(Sort.BY_AREA_ASCENDING);
        assertEquals(tri1, shapes.get(0));
        assertEquals(rec1, shapes.get(1));
        assertEquals(cir1, shapes.get(2));
    }

    @Test
    public void SortAreaByDescending(){
        shapes.sort(Sort.BY_AREA_DESCENDING);
        assertEquals(cir1, shapes.get(0));
        assertEquals(rec1, shapes.get(1));
        assertEquals(tri1, shapes.get(2));
    }

    @Test
    public void SortPerimeterByAscending(){
        shapes.sort(Sort.BY_PERIMETER_ASCENDING);
        assertEquals(tri1, shapes.get(0));
        assertEquals(rec1, shapes.get(1));
        assertEquals(cir1, shapes.get(2));
    }

    @Test
    public void SortPerimeterByDescending(){
        shapes.sort(Sort.BY_PERIMETER_DESCENDING);
        assertEquals(cir1, shapes.get(0));
        assertEquals(rec1, shapes.get(1));
        assertEquals(tri1, shapes.get(2));
    }

    // @Test
    // public void SortArrayByAscendingWithLambda(){
    //     shapes.sort((Measurable a, Measurable b) -> a.area() > b.area() ? 1 : -1);
    //     assertEquals(tri1, shapes.get(0));
    //     assertEquals(rec1, shapes.get(1));
    //     assertEquals(cir1, shapes.get(2));
    // }
}