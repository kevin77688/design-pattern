package org.ntutssl.shapes;

import org.junit.Test;

import static org.junit.Assert.*;

import org.junit.Before;


public class SquareTest{

    private final double delta = 0.001;
    private Square square;

    @Before
    public void Initial(){
        try{
            square = new Square(2);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void GetVariable(){
        assertEquals(2, square.getLength(), delta);
    }

    @Test
    public void Area(){
        assertEquals(4.0, square.area(), delta);
    }

    @Test
    public void Perimeter(){
        assertEquals(8, square.perimeter(), delta);
    }

    @Test
    public void ToString(){
        assertEquals("Square 2.0", square.toString());
    }

    @Test
    public void SquareEdgeBelowZero(){
        try{
            new Square(-2);
            fail();
        } catch (Exception e){
            assertEquals("It's not a square!", e.getMessage());
        }
    }
}