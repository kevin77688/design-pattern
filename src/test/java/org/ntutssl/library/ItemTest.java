package org.ntutssl.library;

import org.junit.Test;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Before;

public class ItemTest 
{
    private Item book1, book2;
    private Item collection1;

    @Before
    public void Initial(){
        book1 = new Book("Book1", "Description1",  "Author1", "0000");
        book2 = new Book("Book2", "Description2",  "Author2", "0001");
        collection1 = new Collection("Collection1", "Collection description1");
    }

    @Test
    public void addBook_ThrowsException(){
        try{
            book1.add(book2);
            fail();
        } catch (Exception e){
            assertEquals("Add item failed!", e.getMessage());
        }
    }

    @Test
    public void collectionGetIsbn_throwException(){
        try{
            collection1.isbn();
            fail();
        } catch (Exception e){
            assertEquals("No isbn found!", e.getMessage());
        }
    }

    @Test
    public void collectionGetAuthor_throwException(){
        try{
            collection1.author();
            fail();
        } catch (Exception e){
            assertEquals("No author found!", e.getMessage());
        }
    }

    @Test
    public void defaultIterator_nullIterator(){
        Iterator<Item> it = book1.iterator();
        assertFalse(it.hasNext());
        try{
            it.next();
            fail();
        } catch (Exception e){
            assertEquals("Null iterator does not point to any element", e.getMessage());
        }
    }
}
