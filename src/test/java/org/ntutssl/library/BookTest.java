package org.ntutssl.library;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class BookTest 
{
    Book book1;
    @Before
    public void Initial(){
        book1 = new Book("Book1", "Description1",  "Author1", "0000");
    }

    @Test
    public void getName(){
        assertEquals("Book1", book1.name());
    }

    @Test
    public void getDescription(){
        assertEquals("Description1", book1.description());
    }

    @Test
    public void getAuthor(){
        assertEquals("Author1", book1.author());
    }

    @Test
    public void getIsbn(){
        assertEquals("0000", book1.isbn());
    }

    @Test
    public void getBookSize(){
        assertEquals(1, book1.size());
    }
}
