package org.ntutssl.library;

import static org.junit.Assert.assertEquals;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

public class CollectionTest 
{
    Item book1, book2, book3;
    Item collection1, collection2;
    @Before
    public void Initial(){
        book1 = new Book("Book1", "Description1",  "Author1", "0000");
        book2 = new Book("Book2", "Description2",  "Author2", "0001");
        book3 = new Book("Book3", "Description3",  "Author3", "0002");
        collection1 = new Collection("Collection1", "Collection description1");
        collection1.add(book1);
        collection1.add(book2);
        collection2 = new Collection("Collection2", "Collection description2");
    }

    @Test
    public void geVariable(){
        assertEquals("Collection1", collection1.name());
        assertEquals("Collection description1", collection1.description());
    }

    @Test
    public void addCollectionIntoCollection(){
        collection2.add(collection1);
        Iterator<Item> it = collection2.iterator();
        int iteratorCount = 0;
        while(it.hasNext()){
            iteratorCount++;
            it.next();
        }
        assertEquals(1, iteratorCount);
    }

    @Test
    public void collectionIteratorCount(){
        Iterator<Item> it = collection1.iterator();
        int iteratorCount = 0;
        while (it.hasNext()){
            iteratorCount++;
            it.next();
        }
        assertEquals(2, iteratorCount);
    }

    @Test
    public void collectionSimpleCount(){
        assertEquals(2, collection1.size());
    }

    @Test
    public void collectionCountCollectionBook(){
        collection2.add(collection1);
        collection2.add(book3);
        assertEquals(3, collection2.size());
    }
}
