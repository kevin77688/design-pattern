package org.ntutssl.library;

import static org.junit.Assert.assertEquals;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

public class LibraryTest 
{
    Item book1, book2, book3;
    Item collection1, collection2;
    Library library1, library2;

    @Before
    public void Initial(){
        book1 = new Book("Book1", "Description1",  "Author1", "0000");
        book2 = new Book("Book2", "Description2",  "Author2", "0001");
        book3 = new Book("Book3", "Description3",  "Author3", "0002");
        collection1 = new Collection("Collection1", "Collection description1");
        collection1.add(book1);
        collection1.add(book2);
        collection2 = new Collection("Collection2", "Collection description2");
        library1 = new Library();
        library1.add(book1);
        library1.add(collection2);
        library2 = new Library();
        library2.add(book3);
        library2.add(collection1);
    }

    @Test
    public void librarySize(){
        assertEquals(1, library1.size());
        assertEquals(3, library2.size());
    }

    @Test
    public void iteratorTest(){
        Iterator<Item> it = library1.iterator();
        assertEquals(book1, it.next());
        assertEquals(collection2, it.next());
        Iterator<Item> it2 = library2.iterator();
        assertEquals(book3, it2.next());
        assertEquals(collection1, it2.next());
    }
}
