package org.ntutssl.library;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class NullIteratorTest 
{
    private NullIterator _nullIterator;

    @Before
    public void Initial(){
        _nullIterator = new NullIterator();
    }

    @Test
    public void hasNextIsFalse(){
        assertFalse(_nullIterator.hasNext());
    }

    @Test
    public void nextThrowException(){
        try{
            _nullIterator.next();
            fail();
        } catch (Exception e){
            assertEquals("Null iterator does not point to any element", e.getMessage());
        }
    }
}
