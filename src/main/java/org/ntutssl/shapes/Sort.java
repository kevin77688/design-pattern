package org.ntutssl.shapes;

import java.util.Comparator;

public class Sort{
    public static Comparator<Measurable> BY_AREA_ASCENDING = new ByAreaAscending();
    public static Comparator<Measurable> BY_AREA_DESCENDING = new ByAreaDescending();
    public static Comparator<Measurable> BY_PERIMETER_ASCENDING = new ByPerimeterAscending();
    public static Comparator<Measurable> BY_PERIMETER_DESCENDING = new ByPerimeterDescending();

    private static class ByAreaAscending implements Comparator<Measurable>{
        public int compare(Measurable firstShape, Measurable secondShape){
            return firstShape.area() > secondShape.area() ? 1 : -1;
        }
    }

    private static class ByAreaDescending implements Comparator<Measurable>{
        public int compare(Measurable firstShape, Measurable secondShape){
            return firstShape.area() < secondShape.area() ? 1 : -1;
        }
    }

    private static class ByPerimeterAscending implements Comparator<Measurable>{
        public int compare(Measurable firstShape, Measurable secondShape){
            return firstShape.perimeter() > secondShape.perimeter() ? 1 : -1;
        }
    }

    private static class ByPerimeterDescending implements Comparator<Measurable>{
        public int compare(Measurable firstShape, Measurable secondShape){
            return firstShape.perimeter() < secondShape.perimeter() ? 1 : -1;
        }
    }
}



