package org.ntutssl.shapes;

public class Rectangle extends Shape {

    public Rectangle(double length, double width) throws Exception {
        if (length <= 0 || width <= 0)
            throw new Exception("It's not a rectangle!");
        _length = length;
        _width = width;
    }

    public double getLength(){
        return _length;
    }

    public double getWidth(){
        return _width;
    }

    public double area(){
        return _length * _width;
    }

    public double perimeter(){
        return 2 * (_length + _width);
    }

    public String toString(){
        return String.format("Rectangle %.1f %.1f", _length, _width);
    }

    private double _length, _width;
}