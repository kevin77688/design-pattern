package org.ntutssl.shapes;

import java.util.ArrayList;
import java.util.Iterator;

public class CompoundShape extends Shape{
    private ArrayList<Shape> shapes;

    public CompoundShape(){
        shapes = new ArrayList<>();
    }

    public double area(){
        double area = 0;
        Iterator<Shape> it = shapes.iterator();
        while(it.hasNext()){
            area += it.next().area();
        }
        return area;
    }

    public double perimeter(){
        double perimeter = 0;
        Iterator<Shape> it = shapes.iterator();
        while(it.hasNext())
            perimeter += it.next().perimeter();
        return perimeter;
    }

    @Override
    public void add(Shape shape){
        shapes.add(shape);
    }

    @Override
    public Shape getChild(int index){
        return shapes.get(index);
    }

    
	public ArrayList<Shape> selectByAreaGreaterThan(double threshold) {
        ArrayList<Shape> result = new ArrayList<>();
        if(this.area() > threshold){
            result.add(this);
        }
        for(Shape shape: shapes){
            if(shape.area() > threshold){
                if(shape instanceof CompoundShape)
                    result.addAll(shape.selectByAreaGreaterThan(threshold));
                else
                    result.add(shape);
            }
        }
        return result;
	}
}
