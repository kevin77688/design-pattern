package org.ntutssl.shapes;

import java.util.ArrayList;
import java.util.Iterator;

public abstract class Shape implements Measurable {

    public void add(Shape shape){
        throw new RuntimeException("Illegal Operation");
    }

    public Shape getChild(int index){
        throw new RuntimeException("Illegal Operation");
    }

    public ArrayList<Shape> selectByAreaGreaterThan(double threshold){
        throw new RuntimeException("Illegal Operation");
    }

    public Iterator<Shape> iterator(){
        return new NullIterator();
    }
}
