package org.ntutssl.shapes;

import java.util.ArrayList;

public final class Geo {
    public static void main(String[] args) {

        InputOutput FileIO = new InputOutput();
        ArrayList<Measurable> shapeList = new ArrayList<Measurable>();
        ArrayList<Measurable> sortedShapeList = new ArrayList<Measurable>();
        try {
            shapeList = FileIO.handleInput(args[0]);
            sortedShapeList = FileIO.handleSort(shapeList, args[2], args[3]);
            FileIO.handleOutput(sortedShapeList, args[1]);

        } catch (Exception e) {
            // e.printStackTrace();
            System.out.println(e);
        }
    }
}
