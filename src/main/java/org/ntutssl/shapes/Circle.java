package org.ntutssl.shapes;

public class Circle extends Shape {
    double PI = Math.PI;

    public Circle(double radius) throws Exception {
        if (radius < 0)
            throw new Exception("It's not a circle!");
        _radius = radius;
    }

    public double getRadius(){
        return _radius;
    }

    public double area(){
        return Math.pow(_radius, 2) * PI;
    }

    public double perimeter(){
        return 2 * _radius * PI;
    }

    public String toString(){
        return String.format("Circle %.1f", _radius);
    }

    private double _radius;
}
