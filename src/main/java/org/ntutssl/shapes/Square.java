package org.ntutssl.shapes;

public class Square extends Shape{

    public Square (double sideLenght) throws Exception {
        if (sideLenght <= 0)
            throw new Exception("It's not a square!");
        this._length = sideLenght;
    }

    public double getLength(){
        return _length;
    }

    public double area(){
        return Math.pow(_length, 2);
    }

    public double perimeter(){
        return 4 * _length;
    }

    public String toString(){
        return String.format("Square %.1f", _length);
    } 

    private double _length;
}