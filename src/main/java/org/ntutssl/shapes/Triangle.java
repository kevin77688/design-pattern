package org.ntutssl.shapes;

import java.util.ArrayList;

public class Triangle extends Shape
{
    public Triangle(ArrayList<TwoDimensionalVector> vectors) throws Exception {
        if (vectors.size() != 3)
            throw new Exception("It's not a triangle!");

        TwoDimensionalVector v1 = vectors.get(0).subtract(vectors.get(1));
        TwoDimensionalVector v2 = vectors.get(1).subtract(vectors.get(2));
        TwoDimensionalVector v3 = vectors.get(2).subtract(vectors.get(0));
        checkParallel(v1, v2);
        checkParallel(v2, v3);
        checkParallel(v3, v1);
        _vectors = vectors;
    }

    private void checkParallel(TwoDimensionalVector u, TwoDimensionalVector v) throws Exception {
        if (u.cross(v) == 0)
            throw new Exception("It's not a triangle!");

    }

    public TwoDimensionalVector at(int index){
        return _vectors.get(index - 1);
    }

    public double area(){
        TwoDimensionalVector v1 = _vectors.get(1).subtract(_vectors.get(0));
        TwoDimensionalVector v2 = _vectors.get(2).subtract(_vectors.get(0));
        return (0.5 * Math.abs(v1.cross(v2)));
    }

    public double perimeter(){
        TwoDimensionalVector v1 = _vectors.get(1).subtract(_vectors.get(0));
        TwoDimensionalVector v2 = _vectors.get(2).subtract(_vectors.get(1));
        TwoDimensionalVector v3 = _vectors.get(0).subtract(_vectors.get(2));
        return v1.length() + v2.length() + v3.length();
    }

    public String toString(){
        String output = "Triangle";
        for (int index = 0; index < 3; index++){
            output += " ";
            output += _vectors.get(index).toString();
        }
        return output;
    }

    private ArrayList<TwoDimensionalVector> _vectors;
}
