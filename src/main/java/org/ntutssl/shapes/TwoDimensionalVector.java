package org.ntutssl.shapes;

public class TwoDimensionalVector {
    public TwoDimensionalVector(int x, int y){
        _x = x;
        _y = y;
    };

    public TwoDimensionalVector(String string){
        String[] inputString = string.split(",");
        if (inputString.length != 2){
            System.out.println("Dimensional error !");
            System.exit(0);
        }
        _x = Integer.parseInt(inputString[0].substring(1));
        _y = Integer.parseInt(inputString[1].substring(0, inputString[1].length() - 1));
    }

    public int getX(){
        return _x;
    }

    public int getY(){
        return _y;
    }

    public String toString(){
        return String.format("[%d,%d]", _x, _y);
    }

    public double length(){
        double squareX = Math.pow(_x, 2);
        double squareY = Math.pow(_y, 2);
        return Math.pow(squareX + squareY, 0.5);
    }

    public int dot(TwoDimensionalVector v){
        return _x * v.getX() + _y * v.getY();
    }

    public int cross(TwoDimensionalVector v){
        return _x * v.getY() - _y * v.getX();
    }

    public TwoDimensionalVector subtract(TwoDimensionalVector v){
        return new TwoDimensionalVector(_x - v.getX(), _y - v.getY());
    }

    private int _x, _y;
}