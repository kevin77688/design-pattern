package org.ntutssl.shapes;

import java.util.ArrayList;

public class ConvexPolygon extends Shape {

    public ConvexPolygon(ArrayList<TwoDimensionalVector> vectors) throws Exception {
        double externalRadian = 0;
        int vectorSize = vectors.size();
        for (int index = 0; index < vectorSize - 2; index++)
            externalRadian += getRadian(vectors.get(index + 1).subtract(vectors.get(index)),
                vectors.get(index + 2).subtract(vectors.get(index + 1)));
        externalRadian += getRadian(vectors.get(0).subtract(vectors.get(vectorSize - 1)),
            vectors.get(vectorSize - 1).subtract(vectors.get(vectorSize - 2)));
        externalRadian += getRadian(vectors.get(1).subtract(vectors.get(0)),
            vectors.get(0).subtract(vectors.get(vectorSize - 1)));
        if (externalRadian - (2 * Math.PI) > 0.01 )
            throw new Exception("It's not a convex polygon!");
        _vectors = vectors;
    }
    

    private double getRadian(TwoDimensionalVector u, TwoDimensionalVector v){
        return Math.acos(u.dot(v) / (u.length() * v.length()));
    } 

    public TwoDimensionalVector at(int index){
        return _vectors.get(index - 1);
    }

    public double area(){
        double totalArea = 0;
        for (int index = 0; index < _vectors.size() - 2 ; index++){
            try{
                ArrayList<TwoDimensionalVector> vectors = new ArrayList<>();
                vectors.add(_vectors.get(0));
                vectors.add(_vectors.get(index + 1));
                vectors.add(_vectors.get(index + 2));
                totalArea += (new Triangle(vectors)).area();
            } catch (Exception e){
                System.out.println(e.getMessage());
            }
            
        }
            // totalArea += Math.abs((_vectors.get(0).getX() * (_vectors.get(index + 1).getY() - 
            // _vectors.get(index + 2).getY()) + _vectors.get(index + 1).getX() * (_vectors.get(index + 2).getY() - 
            // _vectors.get(0).getY()) + _vectors.get(index + 2).getX() * (_vectors.get(0).getY() - 
            // _vectors.get(index + 1).getY())) / 2.0f);
        return totalArea;
    }

    public double perimeter(){
        double answer = 0;
        for (int index = 0 ; index < _vectors.size() - 1 ; index++)
            answer += (_vectors.get(index).subtract(_vectors.get(index + 1))).length();
        answer += _vectors.get(_vectors.size() - 1).subtract(_vectors.get(0)).length();
        return answer;
    }

    public String toString(){
        String output = "ConvexPolygon";
        for (int index = 0; index < _vectors.size(); index++){
            output += " ";
            output += _vectors.get(index).toString();
        }
        return output;
    }
    private ArrayList<TwoDimensionalVector> _vectors;
}