package org.ntutssl.shapes;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class InputOutput {
    
    public InputOutput() { }
    
    public ArrayList<Measurable> handleInput(String inputFileName) throws Exception {

        ArrayList<Measurable> shapeList = new ArrayList<Measurable>();

        try {
            File file = new File(inputFileName);
            Scanner fileScanner = new Scanner(file);
            ArrayList<TwoDimensionalVector> vectors;
            
            while (fileScanner.hasNextLine()) {
                String[] tokens = fileScanner.nextLine().split(" ");
                switch(tokens[0]){
                    case "Circle":
                        if (tokens.length != 2)
                            throw new Exception("It's not a circle!");
                        shapeList.add(new Circle(Double.parseDouble(tokens[1])));
                        break;
                    case "Rectangle":
                        if (tokens.length != 3)
                            throw new Exception("It's not a rectangle!");
                        shapeList.add(new Rectangle(Double.parseDouble(tokens[1]), Double.parseDouble(tokens[2])));
                        break;
                    case "Triangle":
                        if (tokens.length != 4)
                            throw new Exception("It's not a triangle!");
                        vectors = new ArrayList<TwoDimensionalVector>();
                        for (int index = 1; index <= 3; index++)
                            vectors.add(new TwoDimensionalVector(tokens[index]));
                        shapeList.add(new Triangle(vectors));
                        break;
                    case "ConvexPolygon":
                        if (tokens.length < 4)
                            throw new Exception("It's not a convex polygon!");
                        vectors = new ArrayList<TwoDimensionalVector>();
                        for (int index = 1; index < tokens.length; index++)
                            vectors.add(new TwoDimensionalVector(tokens[index]));
                        shapeList.add(new ConvexPolygon(vectors));
                        break;
                }
            }
            fileScanner.close();

          } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        // for (Measurable measurable : shapeList) 
        //     System.out.println(measurable.toString());
        return shapeList;

    }

    public ArrayList<Measurable> handleSort(ArrayList<Measurable> measurables, String compare, String order){
        for (Measurable measurable : measurables) {
            System.out.println(measurable.toString());
        }
        System.out.println("End line \n");
        if (compare.equals("area"))
            if (order.equals("inc"))
                measurables.sort(Sort.BY_AREA_ASCENDING);
            else if (order.equals("dec"))
                measurables.sort(Sort.BY_AREA_DESCENDING);
        else if (compare.equals("perimeter"))
            if (order.equals("inc"))
                measurables.sort(Sort.BY_PERIMETER_ASCENDING);
            else if (order.equals("dec"))
                measurables.sort(Sort.BY_PERIMETER_DESCENDING);
        return measurables;
    }

    public void handleOutput(ArrayList<Measurable> measurables, String outputFileName){
        try{
            File file = new File(outputFileName);
            if (file.exists())
                file.delete();
            file.createNewFile();
            FileWriter writer = new FileWriter(file);
            for (Measurable measurable : measurables) {
                writer.write(measurable.toString() + "\n");
            }
            writer.close();
        } catch (Exception e){
            System.out.println(e);
        }
        
    }
}
