package org.ntutssl.library;

import java.util.Iterator;
import java.util.Vector;

public class Library 
{
    private Vector<Item> _itemList;

    public Library(){
        _itemList = new Vector<>();
    }

    public void add(Item item){
        _itemList.add(item);
    }

    public int size(){
        int totalSize = 0;
        Iterator<Item> it = this.iterator();
        while(it.hasNext())
            totalSize += it.next().size();
        return totalSize;
    }

    public Iterator<Item> iterator(){
        return _itemList.iterator();
    }

	public String findByName(String name){
        return null;
    }
}
