package org.ntutssl.library;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class NullIterator implements Iterator<Item> {
    
    public boolean hasNext(){
        return false;
    }

    public Item next(){
        throw new NoSuchElementException("Null iterator does not point to any element");
    }
}
