package org.ntutssl.library;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Iterator;

public class InputOutput {
    private Library library;
    private boolean isExit;
    private Book createdBook;

    public InputOutput(){
        library = new Library();
        isExit = false;
    }

    public void printLibraryInstructions(){
        while (!isExit){
            try{
                System.out.println("Please enter the instruction as following to manage the library:");
                System.out.println("\t1. 'Add book': to add book to the library");
                System.out.println("\t2. 'Add collection': to add a collection to the library");
                System.out.println("\t3. 'list': to list all the items name in the library");
                System.out.println("\t4. 'list all': to list the detail of all the items in the library");
                System.out.println("\t5. 'exit': to exit the program.");
                String line=new BufferedReader(new InputStreamReader(System.in)).readLine();
                checkStringEmpty(line);
                handleLibraryInstructions(line, library);
            } catch (Exception e){
                System.out.println(e.getMessage());
            }
        }
    }

    private void handleLibraryInstructions(String instruction, Library library){
        switch(instruction){
            case "Add book":
                library.add(addBookInstructions());
                break;
            case "Add collection":
                library.add(addCollectionInstructions());
                break;
            case "list":
                listBooksInfo(library);
                break;
            case "list all":
                listBooksInfoDetail(library);
                break;
            case "exit":
                isExit = true;
                break;
            default:
                System.out.println("Command not found ! please try again.");
                break;
        }
    }

    private Item addBookInstructions(){
        String name, description, author, isbn;
        try{
            System.out.println("Please enter the informations of books:");
            System.out.print("Name of book: ");
            name = new BufferedReader(new InputStreamReader(System.in)).readLine();
            checkStringEmpty(name);
            System.out.print("Description of book: ");
            description = new BufferedReader(new InputStreamReader(System.in)).readLine();
            checkStringEmpty(description);
            System.out.print("Author of book: ");
            author = new BufferedReader(new InputStreamReader(System.in)).readLine();
            checkStringEmpty(author);
            System.out.print("ISBN of book: ");
            isbn = new BufferedReader(new InputStreamReader(System.in)).readLine();
            checkStringEmpty(isbn);
            createdBook = new Book(name, description, author, isbn);
            System.out.println("Book " + createdBook.name() + " added\n");
        } catch (Exception e){
            throw new RuntimeException("Book input error ! please try again.");
        }
        return createdBook;
    }

    private Item addCollectionInstructions(){
        Collection createdCollection;
        String name, description;
        try{
            System.out.println("Please enter the infomations of collection:");
            System.out.print("Name of collection: ");
            name = new BufferedReader(new InputStreamReader(System.in)).readLine();
            checkStringEmpty(name);
            System.out.print("Description of collection: ");
            description = new BufferedReader(new InputStreamReader(System.in)).readLine();
            checkStringEmpty(description);
            createdCollection = new Collection(name, description);
            printCollectionInstructions(createdCollection);
            System.out.println("Collection " + createdCollection.name() + " added\n");
        } catch (Exception e){
            throw new RuntimeException("Collection input error ! please try again. ");
        }
        return createdCollection;
    }

    private void printCollectionInstructions(Collection collection){
        boolean isCollectionExit = false;
        while (!isCollectionExit){
            System.out.println("Please enter the instruction as following to manage the collection:");
            System.out.println("\t1. 'Add book': to add book to the collection");
            System.out.println("\t2. 'Add collection': to add a collection to the collection");
            System.out.println("\t3. 'exit': to exit the process.");
            try{
                String line=new BufferedReader(new InputStreamReader(System.in)).readLine();
                checkStringEmpty(line);
                isCollectionExit = handleCollectionInstructions(line, collection);
            } catch (Exception e){
                System.out.println(e.getMessage());
            }
        }
    }

    private boolean handleCollectionInstructions(String instruction, Collection collection){
        switch (instruction){
            case "Add book":
                collection.add(addBookInstructions());
                break;
            case "Add collection":
                collection.add(addCollectionInstructions());
                break;
            case "exit":
                return true;
            default:
                throw new RuntimeException("Command not found ! please try again");
        }
        return false;
    }
    
    public void findItems(Library library){

    }
    
    private void listBooksInfo(Library library){
        Iterator<Item> it = library.iterator();
        while (it.hasNext())
            it.next().getName();
    }
    
    private void listBooksInfoDetail(Library library){
        Iterator<Item> it = library.iterator();
        while (it.hasNext())
            it.next().detail();
    }

    private void listItemInfo(Item item){
        
    }
    
    private void checkStringEmpty(String inputString){
        if (inputString.isEmpty())
            throw new RuntimeException("Input cannot be empty or blank ! please try again");
    }
}
