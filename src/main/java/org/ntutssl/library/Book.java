package org.ntutssl.library;

public class Book extends Readable
{
    private String _name, _description, _author, _isbn;

    public Book (String name, String description, String author, String isbn){
        _name = name;
        _description = description;
        _author = author;
        _isbn = isbn;
    }
    
    public String name(){
        return _name;
    }

    public String description(){
        return _description;
    }

    public String author(){
        return _author;
    }

    public String isbn(){
        return _isbn;
    }

    public int size(){
        return 1;
    }

    public void getName(){
        System.out.println(this.name());
    }

    public void detail(){
        System.out.println("Book Name: " + this.name());
        System.out.println("\t\tAuthor: " + this.author());
        System.out.println("\t\tDescription: " + this.description());
        System.out.println("\t\tISBN: " + this.isbn());
    }

    public void accept(Visitor visitor){
        
    }

}
