package org.ntutssl.library;

import java.util.Iterator;
import java.util.Vector;

public class Collection extends Readable
{
    private String _name, _description;
    private Vector<Item> _itemList = new Vector<>();

    public Collection (String name, String description){
        this._name = name;
        this._description = description;
    }

    public String name(){
        return _name;
    }

    public String description(){
        return _description;
    }

    public void add(Item item){
        _itemList.add(item);
    }

    public int size(){
        int totalSize = 0;
        Iterator<Item> it = this.iterator();
        while (it.hasNext()){
            totalSize += it.next().size();
        }
        return totalSize;
    }

    public Iterator<Item> iterator(){
        return _itemList.iterator();
    }

    public void getName(){
        Iterator<Item> it = this.iterator();
        System.out.println(this.name());
        while(it.hasNext()){
            it.next().getName();
        }
    }

    public void detail(){
        System.out.println("Collection Name: " + this.name());
        System.out.println("\t\tDescription: " + this.description());
        Iterator<Item> it = this.iterator();
        while(it.hasNext())
            it.next().detail();
    }

    public void accept(Visitor visitor){
        
    }
}
