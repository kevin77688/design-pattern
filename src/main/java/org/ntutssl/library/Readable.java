package org.ntutssl.library;

import java.util.Iterator;

public abstract class Readable implements Item 
{
    public void add (Item items){
        throw new RuntimeException("Add item failed!");
    }

    public String isbn(){
        throw new RuntimeException("No isbn found!");
    }

    public String author(){
        throw new RuntimeException("No author found!");
    }

    public Iterator<Item> iterator(){
        return new NullIterator();
    }
}
